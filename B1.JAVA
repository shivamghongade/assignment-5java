 class DigitFactorial {

    static int factorial(int n) {

        if (n == 0 || n == 1) {
            return 1;

        } else {
            return n * factorial(n - 1);
        }
    }

    public static void main(String[] args) {

        System.out.println("Factorials of digits in the range 1 to 10:");
        for (int i = 1; i <= 10; i++) {

            int digit = i;
            int fact = factorial(digit);
            System.out.println("Factorial of " + digit + " is " + fact);
        }
    }
}
